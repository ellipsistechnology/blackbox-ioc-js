/**
 * Creates an instance of the class and associates it with the give name.
 */
export declare function named(name?: string): (target: Function) => void;
/**
 * Associates the value returned by the factory method with the given name.
 */
export declare function factory(name: string): (prototype: any, propertyKey: string, descriptor: PropertyDescriptor) => void;
/**
 * Assigns the instance associated with the given name to the property.
 */
export declare function autowired(name: string): (target: any, propertyKey: string) => void;
/**
 * Marks a class as providing all services for a given tag.
 */
export declare function serviceClass(tag?: string): (target: Function) => void;
/**
 * Marks a method as provinding the service with the given name.
 */
export declare function service(name: string): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void;
/**
 * Marks a method as provinding the service for the given tag with the given name.
 */
export declare function taggedService(tag: string, name: string): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void;
/**
 * Creates an object with functions for each service with the given tag.
 */
export declare function autowiredService(tag: string): (target: any, propertyKey: string) => void;
declare const rootService: (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void;
declare const rulebaseService: (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void;
export { rootService, rulebaseService };
export declare function callServices(name: string, params?: any): void;
