"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.callServices = exports.rulebaseService = exports.rootService = exports.autowiredService = exports.taggedService = exports.service = exports.serviceClass = exports.autowired = exports.factory = exports.named = void 0;
/**
 * Creates an instance of the class and associates it with the give name.
 */
function named(name) {
    if (name === void 0) { name = undefined; }
    return function (target) {
        container.addNamedClass(name, target);
        container.relink(name);
    };
}
exports.named = named;
/**
 * Associates the value returned by the factory method with the given name.
 */
function factory(name) {
    return function (prototype, propertyKey, descriptor) {
        var instance = prototype[propertyKey]();
        if (typeof instance.then === 'function') {
            instance.then(function (i) {
                container.addNamedInstance(name, i);
                container.relink(name);
            }, function (err) {
                console.error(err);
            });
        }
        else {
            container.addNamedInstance(name, instance);
            container.relink(name);
        }
    };
}
exports.factory = factory;
/**
 * Assigns the instance associated with the given name to the property.
 */
function autowired(name) {
    return function (target, propertyKey) {
        container.wireInstance(name, target, propertyKey);
    };
}
exports.autowired = autowired;
/**
 * Marks a class as providing all services for a given tag.
 */
function serviceClass(tag) {
    if (tag === void 0) { tag = undefined; }
    if (!tag)
        tag = 'default';
    return function (target) {
        container.addNamedClass(tag, target);
        container.addServices(tag);
        container.relink(tag);
    };
}
exports.serviceClass = serviceClass;
/**
 * Marks a method as provinding the service with the given name.
 */
function service(name) {
    return taggedService(undefined, name);
}
exports.service = service;
/**
 * Marks a method as provinding the service for the given tag with the given name.
 */
function taggedService(tag, name) {
    if (!tag)
        tag = 'default';
    return function (target, propertyKey, descriptor) {
        container.addService(tag, target, propertyKey, name);
        container.relink(tag);
    };
}
exports.taggedService = taggedService;
/**
 * Creates an object with functions for each service with the given tag.
 */
function autowiredService(tag) {
    return function (target, propertyKey) {
        container.createServiceForTag(tag, target, propertyKey);
    };
}
exports.autowiredService = autowiredService;
var rootService = service('root-service');
exports.rootService = rootService;
var rulebaseService = service('rulebase-service');
exports.rulebaseService = rulebaseService;
function callServices(name, params) {
    if (params === void 0) { params = undefined; }
    container.callServices(name, params);
}
exports.callServices = callServices;
var Service = /** @class */ (function () {
    function Service(tag, name, instance, methodName) {
        this.instance = instance;
        this.methodName = methodName;
        this.tag = tag;
        this.name = name;
    }
    Service.prototype.call = function (params) {
        return this.instance[this.methodName](params);
    };
    return Service;
}());
var IOCContainer = /** @class */ (function () {
    function IOCContainer() {
        this.delimiter = '.';
        this.services = {};
        this.instances = {};
        this.linkers = {};
    }
    /**
     * Calls all linkers associated with the given label.
     */
    IOCContainer.prototype.relink = function (label) {
        if (this.linkers[label])
            this.linkers[label].forEach(function (linker) { return linker(); });
    };
    /**
     * Add a linking function associated with the given label.
     */
    IOCContainer.prototype.addLinker = function (tag, linker) {
        if (!this.linkers[tag])
            this.linkers[tag] = [];
        this.linkers[tag].push(linker);
    };
    //////////
    // Instance management
    //////////
    IOCContainer.prototype.newInstance = function (type) {
        var instance = {};
        instance['__proto__'] = type.prototype;
        // instance.constructor = type
        type.call(instance);
        return instance;
    };
    IOCContainer.prototype.assertNameDoesntExist = function (name) {
        if (this.instances[name]) {
            console.error(this.instances);
            throw new Error("Named instance '" + name + "' already exists.");
        }
    };
    // TODO test this:
    IOCContainer.prototype.setServiceInstances = function (instance) {
        var _this = this;
        Object.keys(this.services)
            .map(function (serviceName) { return _this.services[serviceName]; })
            .filter(function (service) { return service.instance.constructor.name === instance.constructor.name; })
            .forEach(function (service) { return service.instance = instance; });
    };
    IOCContainer.prototype.addNamedInstance = function (name, instance) {
        this.assertNameDoesntExist(name);
        this.instances[name] = instance;
        this.setServiceInstances(instance);
    };
    IOCContainer.prototype.addNamedClass = function (name, type) {
        var objectName = name ? name : type.name; // default to class name
        this.assertNameDoesntExist(objectName);
        // Add new instance:
        var instance = this.newInstance(type);
        this.instances[objectName] = instance;
        this.setServiceInstances(instance);
    };
    IOCContainer.prototype.wireInstance = function (name, target, propertyKey) {
        var _this = this;
        var linker = function () {
            if (_this.instances[name])
                target[propertyKey] = _this.instances[name];
        };
        linker();
        this.addLinker(name, linker);
    };
    //////////
    // Service management
    //////////
    IOCContainer.prototype.addService = function (tag, instance, key, name) {
        if (name === void 0) { name = undefined; }
        if (!name)
            name = key;
        var serviceName = tag ? tag + this.delimiter + name : name;
        if (!this.services[serviceName])
            this.services[serviceName] = new Service(tag, name, instance, key);
    };
    IOCContainer.prototype.addServices = function (tag) {
        var _this = this;
        if (!this.instances[tag])
            throw new Error("Object named " + tag + " not found.");
        var instance = this.instances[tag];
        Object.keys(instance.constructor.prototype).forEach(function (key) {
            if (typeof instance[key] === 'function')
                _this.addService(tag, instance, key);
        });
    };
    IOCContainer.prototype.createServiceForTag = function (tag, target, propertyKey) {
        var _this = this;
        var linker = function () {
            var services = {};
            Object.keys(_this.services)
                .filter(function (serviceName) { return _this.services[serviceName].tag === tag; })
                .forEach(function (serviceName) {
                var service = _this.services[serviceName];
                services[service.name] = service.instance[service.methodName].bind(service.instance);
            });
            target[propertyKey] = services;
        };
        linker();
        this.addLinker(tag, linker);
    };
    IOCContainer.prototype.callServices = function (tag, params) {
        var _this = this;
        Object.keys(this.services)
            .map(function (serviceName) { return _this.services[serviceName]; })
            .filter(function (service) { return service.tag === tag; })
            .forEach(function (service) { return service.call(params); });
    };
    return IOCContainer;
}());
var container = new IOCContainer();
