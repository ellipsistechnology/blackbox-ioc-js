"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ioc_1 = require("../main/ioc");
//////////
// Blackbox Services
//////////
var GET_WEATHER_TEST = { test: 'getWeather test value' };
var GET_TEMPERATURES_TEST = ['getTemperatures test value'];
var GET_TEMPERATURE_TEST = { test: 'getTemperature test value' };
var GET_HUMIDITIES_TEST = ['getHumidities test value'];
var GET_HUMIDITY_TEST = { test: 'getHumidity test value' };
var WeatherService = /** @class */ (function () {
    function WeatherService() {
    }
    WeatherService.prototype.getWeather = function () {
        return GET_WEATHER_TEST;
    };
    WeatherService.prototype.getTemperatures = function () {
        return GET_TEMPERATURES_TEST;
    };
    WeatherService.prototype.getTemperature = function () {
        return GET_TEMPERATURE_TEST;
    };
    WeatherService.prototype.getHs = function () {
        return GET_HUMIDITIES_TEST;
    };
    WeatherService.prototype.getH = function () {
        return GET_HUMIDITY_TEST;
    };
    __decorate([
        ioc_1.service('getHumidities')
    ], WeatherService.prototype, "getHs", null);
    __decorate([
        ioc_1.taggedService('weather', 'getHumidity')
    ], WeatherService.prototype, "getH", null);
    WeatherService = __decorate([
        ioc_1.serviceClass('weather')
    ], WeatherService);
    return WeatherService;
}());
var Test3 = /** @class */ (function () {
    function Test3() {
    }
    __decorate([
        ioc_1.autowiredService('weather')
    ], Test3.prototype, "weatherService", void 0);
    __decorate([
        ioc_1.autowiredService('default')
    ], Test3.prototype, "defaultService", void 0);
    return Test3;
}());
test('@serviceClass wires all methods to service object', function () {
    var test3 = new Test3();
    expect(test3.weatherService.getWeather()).toBe(GET_WEATHER_TEST);
    expect(test3.weatherService.getTemperatures()).toBe(GET_TEMPERATURES_TEST);
    expect(test3.weatherService.getTemperature()).toBe(GET_TEMPERATURE_TEST);
    expect(test3.weatherService.getHs()).toBe(GET_HUMIDITIES_TEST);
    expect(test3.weatherService.getH()).toBe(GET_HUMIDITY_TEST);
});
test('@service wires methods with default tag', function () {
    var test3 = new Test3();
    expect(test3.defaultService.getHumidities()).toBe(GET_HUMIDITIES_TEST);
});
test('@taggedOperationId wires methods to correct service', function () {
    var test3 = new Test3();
    expect(test3.weatherService.getHumidity()).toBe(GET_HUMIDITY_TEST);
    expect(test3.weatherService.getHumidities).toBeUndefined();
});
//////////
// Non-Blackbox Services
//////////
var NamedTest = /** @class */ (function () {
    function NamedTest() {
        this.str = 'test string';
        this.num = 47;
        this.array = [1, 2, 3, 4, 5];
    }
    NamedTest = __decorate([
        ioc_1.named('named-class')
    ], NamedTest);
    return NamedTest;
}());
var Factory = /** @class */ (function () {
    function Factory() {
    }
    Factory.prototype.factory = function () {
        return new NamedTest();
    };
    Factory.prototype.promiseFactory = function () {
        return Promise.resolve({ test: 'test123' });
    };
    __decorate([
        ioc_1.factory('named-factory')
    ], Factory.prototype, "factory", null);
    __decorate([
        ioc_1.factory('promise-factory')
    ], Factory.prototype, "promiseFactory", null);
    return Factory;
}());
var Wrapper = /** @class */ (function () {
    function Wrapper() {
    }
    __decorate([
        ioc_1.autowired('named-class')
    ], Wrapper.prototype, "namedClass", void 0);
    __decorate([
        ioc_1.autowired('named-factory')
    ], Wrapper.prototype, "namedFactory", void 0);
    __decorate([
        ioc_1.autowired('promise-factory')
    ], Wrapper.prototype, "promiseFactory", void 0);
    return Wrapper;
}());
test("Can autowire a named class", function () {
    var local = new NamedTest();
    var wrapper = new Wrapper();
    expect(wrapper.namedClass).toEqual(local);
});
test("Can autowire an from a factory method", function () {
    var local = new NamedTest();
    var wrapper = new Wrapper();
    expect(wrapper.namedFactory).toEqual(local);
    expect(wrapper.promiseFactory).toEqual({ test: 'test123' });
});
var bTest = false;
var count = 0;
var InitService = /** @class */ (function () {
    function InitService() {
    }
    InitService.prototype.init = function () {
        bTest = true;
    };
    InitService = __decorate([
        ioc_1.serviceClass('init')
    ], InitService);
    return InitService;
}());
var InitService2 = /** @class */ (function () {
    function InitService2() {
    }
    InitService2.prototype.init1 = function () {
        count++;
    };
    InitService2.prototype.init2 = function () {
        count++;
    };
    InitService2.prototype.notInit = function () {
        count = 100;
    };
    __decorate([
        ioc_1.taggedService('init', 'init1')
    ], InitService2.prototype, "init1", null);
    __decorate([
        ioc_1.taggedService('init', 'init2')
    ], InitService2.prototype, "init2", null);
    return InitService2;
}());
test("Calling all services calls all services.", function () {
    ioc_1.callServices('init');
    expect(bTest).toBeTruthy();
    expect(count).toEqual(2);
});
