/**
 * Creates an instance of the class and associates it with the give name.
 */
export function named(name:string = undefined) {
  return function(target: Function) {
    container.addNamedClass(name, target)
    container.relink(name)
  }
}

/**
 * Associates the value returned by the factory method with the given name.
 */
export function factory(name:string) {
  return function(prototype: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const instance = prototype[propertyKey]()
    if(typeof instance.then === 'function') {
      instance.then((i: any) => {
        container.addNamedInstance(name, i)
        container.relink(name)
      },
      (err: any) => {
        console.error(err)
      })
    } else {
      container.addNamedInstance(name, instance)
      container.relink(name)
    }
  }
}

/**
 * Assigns the instance associated with the given name to the property.
 */
export function autowired(name:string) {
  return function(target: any, propertyKey: string) {
    container.wireInstance(name, target, propertyKey)
  }
}

/**
 * Marks a class as providing all services for a given tag.
 */
export function serviceClass(tag:string = undefined) {
  if(!tag)
    tag = 'default'

  return function(target: Function) {
    container.addNamedClass(tag, target)
    container.addServices(tag)
    container.relink(tag)
  }
}

/**
 * Marks a method as provinding the service with the given name.
 */
export function service(name:string) {
  return taggedService(undefined, name)
}

/**
 * Marks a method as provinding the service for the given tag with the given name.
 */
export function taggedService(tag:string, name:string) {
  if(!tag)
    tag = 'default'

  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    container.addService(tag, target, propertyKey, name)
    container.relink(tag)
  }
}

/**
 * Creates an object with functions for each service with the given tag.
 */
export function autowiredService(tag:string) {
  return function(target: any, propertyKey: string) {
    container.createServiceForTag(tag, target, propertyKey)
  }
}

const rootService = service('root-service')
const rulebaseService = service('rulebase-service')
export {rootService, rulebaseService}

export function callServices(name:string, params:any = undefined) {
  container.callServices(name, params)
}

class Service {
  tag:string
  name:string
  instance:any
  methodName:string

  constructor(tag:string, name:string, instance:any, methodName:string) {
    this.instance = instance
    this.methodName = methodName
    this.tag = tag
    this.name = name
  }

  call(params:any):any {
    return this.instance[this.methodName](params)
  }
}

class IOCContainer {
  services: { [key:string]:Service }
  instances: { [key:string]:any }
  linkers: { [key:string]:Function[] }
  delimiter = '.'

  constructor() {
    this.services = {}
    this.instances = {}
    this.linkers = {}
  }

  /**
   * Calls all linkers associated with the given label.
   */
  relink(label:string) {
    if(this.linkers[label])
      this.linkers[label].forEach((linker) => linker())
  }

  /**
   * Add a linking function associated with the given label.
   */
  private addLinker(tag:string, linker:()=>void) {
    if(!this.linkers[tag])
      this.linkers[tag] = []
    this.linkers[tag].push(linker)
  }


  //////////
  // Instance management
  //////////

  private newInstance(type:Function):any {
    let instance:any = {}
    instance['__proto__'] = type.prototype
    // instance.constructor = type
    type.call(instance)
    return instance
  }

  private assertNameDoesntExist(name:string) {
    if(this.instances[name]) {
      console.error(this.instances);
      throw new Error(`Named instance '${name}' already exists.`)
    }
  }

// TODO test this:
  private setServiceInstances(instance:any) {
    Object.keys(this.services)
      .map((serviceName:string) => this.services[serviceName])
      .filter((service:Service) => service.instance.constructor.name === instance.constructor.name)
      .forEach((service:Service) => service.instance  = instance)
  }

  addNamedInstance(name:string, instance:any) {
    this.assertNameDoesntExist(name)
    this.instances[name] = instance
    this.setServiceInstances(instance)
  }

  addNamedClass(name:string, type:any) {
    let objectName = name ? name : type.name // default to class name
    this.assertNameDoesntExist(objectName)

    // Add new instance:
    let instance = this.newInstance(type)
    this.instances[objectName] = instance
    this.setServiceInstances(instance)
  }

  wireInstance(name:string, target:any, propertyKey:string) {
    const linker = () => {
      if(this.instances[name])
        target[propertyKey] = this.instances[name]
    }
    linker()
    this.addLinker(name, linker)
  }


  //////////
  // Service management
  //////////

  addService(tag:string, instance:any, key:string, name:string = undefined) {
    if(!name)
      name = key
    const serviceName = tag ? tag + this.delimiter + name : name
    if(!this.services[serviceName])
      this.services[serviceName] = new Service(tag, name, instance, key)
  }

  addServices(tag:string) {
    if(!this.instances[tag])
      throw new Error(`Object named ${tag} not found.`)
    let instance = this.instances[tag]
    Object.keys(instance.constructor.prototype).forEach( (key:string) => {
      if(typeof instance[key] === 'function')
        this.addService(tag, instance, key)
    })
  }

  createServiceForTag(tag:string, target:any, propertyKey:string) {

    const linker = () => {
      let services = {}

      Object.keys(this.services)
        .filter( (serviceName:string) => this.services[serviceName].tag === tag )
        .forEach( (serviceName:string) => {
          const service = this.services[serviceName]
          services[service.name] = service.instance[service.methodName].bind(service.instance)
        })

      target[propertyKey] = services
    }
    linker()
    this.addLinker(tag, linker)
  }

  callServices(tag:string, params:any) {
    Object.keys(this.services)
      .map((serviceName:string) => this.services[serviceName])
      .filter((service:Service) => service.tag === tag)
      .forEach((service:Service) => service.call(params))
  }
}

const container = new IOCContainer()
