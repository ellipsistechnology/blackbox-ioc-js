import {
  named,
  autowired,
  factory,
  autowiredService,
  serviceClass,
  taggedService,
  service,
  callServices} from '../main/ioc'


//////////
// Blackbox Services
//////////

const GET_WEATHER_TEST = {test: 'getWeather test value'}
const GET_TEMPERATURES_TEST = ['getTemperatures test value']
const GET_TEMPERATURE_TEST = {test: 'getTemperature test value'}
const GET_HUMIDITIES_TEST = ['getHumidities test value']
const GET_HUMIDITY_TEST = {test: 'getHumidity test value'}

@serviceClass('weather')
class WeatherService {
  getWeather():any {
    return GET_WEATHER_TEST
  }

  getTemperatures():any[] {
    return GET_TEMPERATURES_TEST
  }

  getTemperature():any {
    return GET_TEMPERATURE_TEST
  }

  @service('getHumidities')
  getHs():any[] {
    return GET_HUMIDITIES_TEST
  }

  @taggedService('weather', 'getHumidity')
  getH():any {
    return GET_HUMIDITY_TEST
  }
}

class Test3 {
  @autowiredService('weather')
  weatherService: any

  @autowiredService('default')
  defaultService: any
}

test('@serviceClass wires all methods to service object', () => {
  let test3 = new Test3()
  expect(test3.weatherService.getWeather()).toBe(GET_WEATHER_TEST)
  expect(test3.weatherService.getTemperatures()).toBe(GET_TEMPERATURES_TEST)
  expect(test3.weatherService.getTemperature()).toBe(GET_TEMPERATURE_TEST)
  expect(test3.weatherService.getHs()).toBe(GET_HUMIDITIES_TEST)
  expect(test3.weatherService.getH()).toBe(GET_HUMIDITY_TEST)
})

test('@service wires methods with default tag', () => {
  let test3 = new Test3()
  expect(test3.defaultService.getHumidities()).toBe(GET_HUMIDITIES_TEST)
})

test('@taggedOperationId wires methods to correct service', () => {
  let test3 = new Test3()
  expect(test3.weatherService.getHumidity()).toBe(GET_HUMIDITY_TEST)
  expect(test3.weatherService.getHumidities).toBeUndefined()
})


//////////
// Non-Blackbox Services
//////////

@named('named-class')
class NamedTest {
  str: string
  num: number
  array: number[]

  constructor() {
    this.str = 'test string'
    this.num = 47
    this.array = [1,2,3,4,5]
  }
}

class Factory {
  @factory('named-factory')
  factory():NamedTest {
    return new NamedTest()
  }

  @factory('promise-factory')
  promiseFactory(): Promise<any> {
    return Promise.resolve({test: 'test123'})
  }
}

class Wrapper {
  @autowired('named-class')
  namedClass: NamedTest

  @autowired('named-factory')
  namedFactory: NamedTest

  @autowired('promise-factory')
  promiseFactory: any
}

test("Can autowire a named class", () => {
  const local = new NamedTest()
  const wrapper = new Wrapper()
  expect(wrapper.namedClass).toEqual(local)
})

test("Can autowire an from a factory method", () => {
  const local = new NamedTest()
  const wrapper = new Wrapper()
  expect(wrapper.namedFactory).toEqual(local)
  expect(wrapper.promiseFactory).toEqual({test: 'test123'})
});

let bTest = false;
let count = 0;
@serviceClass('init')
class InitService {
  init() {
    bTest = true
  }
}

class InitService2 {
  @taggedService('init', 'init1')
  init1() {
    count++
  }

  @taggedService('init', 'init2')
  init2() {
    count++
  }

  notInit() {
    count = 100
  }
}

test("Calling all services calls all services.", () => {
  callServices('init')

  expect(bTest).toBeTruthy()
  expect(count).toEqual(2)
})
